# User Guide
 The Visual map plugin is a tool to better place your LEDs individually.  
 With it you can have better visual for effects.  
 You can create a whole map of your RGB setup or multiple ones to fine tune any controller.  

# Main Window Options

At the left there is the list of your controllers. Note that you will maybe need to configure fist the number of LEDs in the OpenRGB main window for some controllers before using this plugin. This is the case for eg. some Corsair Controllers.

At the center there is the map. This is here that you will place your items.

At the right there is some options for item manipulation.

You can save a map, load one or clear the actual with the buttons at the right.

New map : Create a blank map. <br>
Plugin info : Shows some information about the plugin and some buttons to download the last version or going into the OpenRGB plugin folder. <br>
Register controller: Create a virtual device with the map you created that OpenRGB can use. <br>
Add background: Add a background to your map. This can be pre-made colors or presets or your own files <br>

<h4>Grid Options</h4>
Auto load: Load the map at the start of OpenRGB. <br>
Auto register: Register a virtual device based of your map at the start of OpenRGB. <br>
Unregister member contollers: Will erase the original controller. Useful when you fuse two or more controllers or you want only your configuration used by OpenRGB for the controller you are modifying. <br>
Show grid: Show a grid in your map to help positioning. <br>
Show bounds: Show a line at the bounds of your map. <br>
Live preview: If an effect or color is running, it will display it on your map. <br>  
Width and Height: Configure width and height of your map. <br>
Auto resize: Will resize the width and height of your map automatically. <br>  

<h4>Item options, when an item is selected</h4>

x: Change the horizontal positioning of your item. <br>
y: Change the vertical positioning of your item. <br>
Shape: Organize the LEDS of the item in the shape you selected (vertical, horizontal, custom). <br>
Edit shape: Open the shape editor. <br>
Identify: Light the selected LEDs of the item in green on your real hardware. <br>

<h4>Shape Editor</h4>
Width/Height: Modify the width and height of the item modified.<br>
LEDs: Show the total of LEDs of the modified item. <br>
Undo: Undo the last actions you did on the item. <br>
Rotate: Rotate the selected LEDs in a clockwise 90° direction. <br> 
H-Flip: Do a horizontal flip of the selected LEDs of the item. <br>
V-Flip: Do a vertical flip of the selected LEDs of the item. <br>
H-Line: Organize the selected LEDs in a horizontal bar. <br>
V-Line: Organize the selected LEDs in a vertical bar. <br>
Square: Organize the selected LEDs in a square shape. <br>
Circle: Organize the selected LEDs in a round shape. <br>
ZigZag: Organize the selected LEDs in a zigzag pattern (broken line). You can define the height of the shape. <br>
Sawtooth: Organize the selected LEDs in a sawtooth pattern (one down, one up). You can define the height of the shape. <br>  
Grow: Grows the shape of the selected LEDs. <br>
Shrink: Shrink the shape of the selected LEDs. <br>
Auto resize: Automatically adapt the width and height of your item. <br>  
Copy shape: when you have a similar device zone, do a copy of the shape to the second zone. <br> 
Identify: Shows in your hardware green lights to identify the LEDs you are working on. <br>
Auto identify: Same, but automatically when you select LEDs. <br>

<h4>Background Editor</h4>

You can do some default lighting for the items you worked on in the visual map. In the custom tab you can define some pre-made
presets or create (and save/load) your own. In the File tab you can select your own image file as a background.  