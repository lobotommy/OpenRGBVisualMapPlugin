#ifndef GRIDSETTINGS_H
#define GRIDSETTINGS_H

struct GridSettings
{
    int w;
    int h;
    bool show_grid;
    bool show_bounds;
    bool live_preview;
    int grid_size;
    bool auto_load;
    bool auto_register;
    bool unregister_members;
};

#endif // GRIDSETTINGS_H
